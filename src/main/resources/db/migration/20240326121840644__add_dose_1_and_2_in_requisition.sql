-- Define new columns in available_requisition_columns table
INSERT INTO requisition.available_requisition_columns
(id, name, label, indicator, mandatory, isDisplayRequired, canChangeOrder, canBeChangedByUser, columnType, definition)
VALUES
    ('aef84e58-a872-4b5f-bc06-2debd50b3bc5', 'firstDose', 'Dose 1', 'PA', false, false, true, true, 'NUMERIC', 'The number of doses required'),
    ('1a79bfc4-4ab1-4b3d-82c7-4e4e3f8eb9c7', 'secondDose', 'Dose 2', 'PB', false, false, true, true, 'NUMERIC', 'The number of doses required');

-- Insert values into available_requisition_column_sources
INSERT INTO requisition.available_requisition_column_sources (columnId, value)
VALUES
    ('aef84e58-a872-4b5f-bc06-2debd50b3bc5', 'USER_INPUT'),
    ('1a79bfc4-4ab1-4b3d-82c7-4e4e3f8eb9c7', 'USER_INPUT');

-- Update requisition_line_items table again to add new columns
ALTER TABLE requisition.requisition_line_items
    ADD COLUMN firstDose INTEGER,
    ADD COLUMN secondDose INTEGER;

-- Update columns_maps to include new columns in all requisition templates
INSERT INTO requisition.columns_maps
(requisitiontemplateid, requisitioncolumnid, definition, displayorder, indicator, isdisplayed, label, name, requisitioncolumnoptionid, source, key, tag)
SELECT
    t.id,
    a.id,
    a.definition,
    CASE a.name
        WHEN 'firstDose' THEN 13
        WHEN 'secondDose' THEN 14
        END AS displayorder,
    a.indicator,
    false,
    a.label,
    a.name,
    NULL,
    1,
    a.name,
    NULL
FROM
    requisition.requisition_templates AS t
        INNER JOIN (
        SELECT requisitiontemplateid, count(*)
        FROM requisition.columns_maps
        GROUP BY requisitiontemplateid
    ) AS c ON c.requisitiontemplateid = t.id
        INNER JOIN requisition.available_requisition_columns AS a ON a.name IN ('firstDose', 'secondDose');
